#!/bin/bash

from :  https://github.com/renpy/renpyweb/blob/master/README.md

git clone https://github.com/renpy/renpy-build/
cd renpy-build/

export BASE=`source ./nightly/git.sh`

# fix-up tasks ordering
./build.py --platform web

#rm -rf build/ install/ python-emscripten/2.7.18/build/
make cythonclean
emcc --clear-ports

make
scripts/install_in_renpy.sh

make testserver
echo $BROWSER http://localhost:8000/

