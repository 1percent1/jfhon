#!/usr/bin/env python3
import json
from flask import Flask, render_template
from flask_htmx import HTMX, make_response

_app = Flask(__name__)
_htmx = HTMX(_app)

@_app.route("/")
def home():
  url = _htmx.current_url
  return render_template("index.html",current_url=url)

@_app.route("/hola-mundo")
def hola_mundo():
  body = "Hola Mundo!"
  resp = make_response(body)
  resp.headers["HX-Push-URL"] = "false"
  trigstr = json.dumps({"event1":"A message","event2":"Another message"})
  resp.headers["HX-Trigger"] = trigstr
  return resp

@_app.route("/hola")
def hola():
  body = "Hola!"
  trig={"event1": "A message", "event2": "Another message"}
  resp = make_response(body,push_url=False,trigger=trig)
  return resp

@_app.route("/hello")
def hello():
  return "<p>Hello!</p>"

if __name__ == '__main__': 
  """
  https://flask-htmx.readthedocs.io/en/latest/quickstart.html
  pip3 install flask-htmx
  """
  _app.run(host="0.0.0.0", port=5555)

