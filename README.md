# jfhon

## David B Hon only son of John Francis Hon and Dinorah Carmen Esteva

Started my own memoirs -- TBD create a renpy visual novel and include Dinorah, Angeles, Salvador, Leon and Kevin and Karin and Alan and Bob Price and Mike Hollis, Jay and Bob Kozon, Richard Elston, Mikey V; and Joe Sirna at Columbia and Brooklyn Sunday feasts

## Setup 3 host Openstack system via https://ubuntu.com/openstack/install

According to recent Openstack installation notes, hosts should have at least 2 drives with 300GB or more each of free space...

https://ubuntu.com/openstack/install  and  https://docs.openstack.org/install-guide/

    New host for openstack and maybe cloudstack:
    s220144: inet 69.30.255.210/29 brd 69.30.255.215 scope global ens9
    df -h|grep 'dev/sd'
    /dev/sdb2       440G   19G  398G   5% /
    /dev/sda        440G   28K  417G   1% /sda

New host hiyield.us replaces 90ghz.org ... ensuring Openstack compat 

    hon@90ghz: inet 198.204.224.250/29 brd 198.204.224.255 scope global ens9
    /dev/sda5       961G  436G  477G  48% /
    /dev/sda1       960M  375M  537M  42% /boot

    hon@wehike: inet 69.30.253.2/29 brd 69.30.253.7 scope global enp6s0
    /dev/sda5       116G   44G   67G  40% /
    /dev/sda1       960M  208M  703M  23% /boot
    /dev/sdb        2.7T  2.0T  656G  75% /sdb

    hon@1percent: inet 69.30.230.122/29 brd 69.30.230.127 scope global ens9
    df | grep dev
    /dev/sdb2       880G   26G  809G   4% /
    tmpfs            63G     0   63G   0% /dev/shm
    /dev/sda        880G   28K  835G   1% /sda

Note the ubuntu multi host example indicates 3 baremetal hosts ,,,  

    ... three machines, each of which has the following requirements:

    physical machine running Ubuntu 22.04 LTS
    a multi-core amd64 processor (ideally with 4+ cores)
    a minimum of 32 GiB of free memory
    200 GiB of SSD storage available on the root disk
    a least one un-partitioned disk of at least 200 GiB in size
    two network interfaces
    primary: for access to the OpenStack control plane
    secondary: for remote access to cloud VMs
    Caution: Any change in IP address of any machine will be detrimental to the deployment. Dedicated physical machines with fixed IP   address allocations are therefore required.

    Machine names
    For the purpose of this tutorial, the following machine names are used:

    MACHINE	FQDN	UN-PARTITIONED DISK
    sunbeam01	sunbeam01.example.com	/dev/sdb
    sunbeam02	sunbeam02.example.com	/dev/sdb
    sunbeam03	sunbeam03.example.com	/dev/sdb

Only our wholesaleinternet host 1percent.us meets above reqs:

    root@1percent:~# df -h
    Filesystem      Size  Used Avail Use% Mounted on
    tmpfs            13G  1.8M   13G   1% /run
    /dev/sdb2       880G   26G  809G   4% /
    tmpfs            63G     0   63G   0% /dev/shm
    tmpfs           5.0M     0  5.0M   0% /run/lock
    tmpfs            13G  4.0K   13G   1% /run/user/1001
    /dev/sda        880G   28K  835G   1% /sda

... so let's lease 2 new hosts:

Dual Xeon E5-2670 96GB 2x480GB SSD ...
and once we have out 3-host-cloud, and everything moved from wehike.us and 90ghz.org ...
yadda yadda ...

## Init jfhon.1percent.us

Create a visual novel using renpy content from 93d b-day jpg and mp4 ...


    index.html
    flaskhtmx.py
    rndpy_tut.rpy


## Refs

https://www.renpy.org/doc/html/quickstart.html

https://www.renpy.org/doc/html/web.html

wget https://www.renpy.org/dl/8.2.0/renpy-8.2.0-sdk.tar.bz2

https://us-east-1.console.aws.amazon.com/console/home?region=us-east-1

## Gitlab Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/1percent1/jfhon.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/1percent1/jfhon/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
