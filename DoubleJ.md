# John Francis Hon 2nd trophy wife Jodith C Hon

Found this Pauma Valley item from officialusa.com ...

## Judith C Hon

Pauma Valley, CA

Nurse (Registered) is an occupation. Judith will celebrate 86th birthday on May 12. Judith lives at 16045 El Tae Rd, Pauma Valley, CA at present. We know that Helen F Dooley, John F Hon, and two other persons also lived at this address, perhaps within a different time frame. (760) 742-1747 (Pacific Bell) is the number currently linked to Judith. John Francis Hon, John F Hon were identified as possible owners of the phone number (760) 742-1747

Main Address

16045 El Tae Rd, Pauma Valley, CA 92061

Condo, Attached Garage

Three bedrooms, Three bathrooms

Lot Size - 11.85 acres, Floor Size - 2,124 sqft

Parcel ID# 1324706509

Last Sale Nov 2009 - Price $365,000

County: San Diego County

FIPS: 60730191062012

Possible connections via main address - John F Hon

Latitude, Longitude: 33.304048, -116.984491

Phone (760) 742-1747

Possible connections via phone numbers - John Francis Hon, John F Hon

