#!/bin/bash
jdate=`date "+%Y:%j:%H:%M:%S"`
git clone --depth 1 https://github.com/matthewwithanm/python-markdownify.git
echo
echo -------------------------------------------------------------------------
echo
echo $jdate
python3 -m venv py3env
source py3env/bin/activate
markdownify DavidB.HonRecallsLife1956_2024.html > DavidB.HonRecallsLife1956_2024.md
cp -p DavidB.HonRecallsLife1956_2024.html $jdate_DavidB.HonRecallsLife1956_2024.html
cp -p DavidB.HonRecallsLife1956_2024.md $jdate_DavidB.HonRecallsLife1956_2024.md
\ls -alqhF *.html *.md

