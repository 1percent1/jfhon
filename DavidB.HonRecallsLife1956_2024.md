DavidB.HonRecallsLIfe1956\_2024

Earliest Childhood:  

 My very earliest memory is hearing my mother speaking in spanglish
 and pointing out the window/portal of an  

 airplane. I'm guesing the airplane was large -- big enough to travel
 from SoCal LAX to Miami in the late 1950s.  

 "Mira mira mira los clouds" and she pointed out the portal. I
 responed "a ver a ver?" Decades later when I  

 mentioned the memory to my mother, she gasped ...  was it our
 flight out of LA when I was 2-ish years old  

 (1958-59ish)? Dad had decided to divorce her, so she packed a bag
 and rounded up me and my sister Diana.  

 Only after we boarded the jet plane, mom realized she had neglected
 to pack a milk bottle for me. As I became  

 hungry on the long flight, she distracted me by having me sit at the
 window and watch the clouds.  


 I believe she found her way back briefly to her Cuba homes -- one in
 Havana (the lost city) and the other at the  

 plantation. The is a 2005 film entitled "The Lost City" which is a
 good portrayal of my mother's family tragedy:  

[Havana Cuba -- The Lost City](https://en.wikipedia.org/wiki/The_Lost_City_(2005_film) "https://en.wikipedia.org/wiki/The_Lost_City_(2005_film)")  

 Mama told me I gained a lot of weight during that brief sojourn in
 her homeland, as I demanded countless  

 bottles of milk. Evidently I had learned a lesson  about hunger
 on the flight...  

  

 On Abuelo's  plantation I recall riding on a horse, my arms
 wrapped around him  from behind as Abuelo  

 (Salvado Esteva) made the rounds about his combination plantation
 and dairy farm. Many years later I asked  

 mom (Dinorah Carmen Esteva) about the location of the Abuelo's
 plantation ... "the interior, well east of Havana".  

 I also asked mom about her mother (my Abuela), who I learned was a
 pharmacist. Abuela was "from money", I   

 believe -- a family of physicians and business folk. Abuela was a
 lover of Operah, and her 2 most favorites were  

 the French operas Dinorah and Carmen -- thus mom's name. I remember
 being alone in my crib one day and  

 hearing the adults down the hall from my room chatting. I called out
 repeatedly for something to drink, feeling quite  

 thirsty. Despite calling out repeatedly, no one responded. Evidently
 this was my first experience with "tough love".  

 So I somehow climbed out of the crib and marched down the hall to
 the kitchen, with the idea of finding something  

 to drink. Someone noticed me and I heard gasps of surprise. I was
 scolded for by actions, but eventually received  

 my drink.  

  

 I remember having a pet chicken at some point during our bruef stay
 in Cuba, perhaps I had begged Abuelo to   

 not slaughter it. I recall a vivid memory standing outside the
 farmhouse, watching him grab a chicken by the neck  

 and  holding it on a chopping block. He swung a hand axe and
 chopped off its head. The hapless headless chicken  

 slipped out of his grip and ran around the yard squirting blood as
 he cursed. Once it collapsed he swept it up and  

 we walked into the kitchen to hand the very fresh meat to Abuela. In
 the kitchen Abuela was holding out a glass of  

 fresh warm milk from their dairy for me. I took a swallow and then
 vomited. Abuelo cursed again.  

  

  

 Pets: Happy, and Lady Chica, Rubia Linda, Jake and Kelly nd Tammy,
 Sammi, Lexi, Krissie, Tammy2, Holly and Hodges, Kobe,,  

 Jax and GrahamCracker and BailieBoy  

  

 2014 Holidays: Holly and Hodges  

![](blob:https://account.microsoft.com/4ca2856e-cdd3-438a-810a-00463cb1d07e)  
  

![HollyHodges2014](../Downloads/HollyHodges2014.jpg)  

  

  
