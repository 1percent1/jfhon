## Ren'Py language using the `pre/code` in Markdown

### Using pre/code:

<pre><code>
```renpy
# Ren'Py statements
```
</code></pre>

### Demo:

```renpy
init:

    # Set up the size of the screen, and the window title.
    $ config.screen_width = 800
    $ config.screen_height = 600
    $ config.window_title = "The Ren'Py Demo Game"

    # Declare the images that are used in the program.

    # Backgrounds.
    image bg carillon = "carillon.jpg"
    image bg whitehouse = "whitehouse.jpg"
    image bg washington = "washington.jpg"
    image bg onememorial = "1memorial.jpg"
    image black = Solid((0, 0, 0, 255))

    # Character pictures.
    image eileen happy = "9a_happy.png"
    image eileen vhappy = "9a_vhappy.png"
    image eileen concerned = "9a_concerned.png"
```
